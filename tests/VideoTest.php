<?php


/**
 * This File tests the video controller
 *
 * @author beij
 */
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\VideoController;
use Alaouy\Youtube\Youtube;

class VideoTest extends TestCase {

    //resets the database after each test

    use DatabaseMigrations;
    use WithoutMiddleware;

    public function testRoot() {
        $this->call('GET', '/video');
        $this->assertResponseOk();
    }

    protected function verifyUrl($url) {
        $response = $this->action('GET', 'VideoController@verification', array('url' => $url));
        $decodedResponse = json_decode($response->content());
        return $decodedResponse;
    }

    public function testValidUrlVerfication() {

        $decodedResponse = $this->verifyUrl('https://www.youtube.com/watch?v=tc4ROCJYbm0');
        $this->assertTrue($decodedResponse->verification);
    }

    public function testInvalidUrlVerfication() {

        $decodedResponse = $this->verifyUrl('https://www.youtube.com/watch?v=tc4asdROCJYbm0');
        $this->assertFalse($decodedResponse->verification);
    }

    public function testPlayerCodeCreation() {
        $response = $this->action('GET', 'VideoController@getPlayerById', array('youtubeid' => 'b-Cr0EWwaTk'));
        $this->assertEquals($response->content(), '<iframe width="640" height="360" src="//www.youtube.com/embed/b-Cr0EWwaTk?autoplay=1" frameborder="0" allowfullscreen></iframe>');
    }
    
    public function testStoreUrl(){
        $response = $this->action('POST', 'VideoController@store', array('url' => 'https://www.youtube.com/watch?v=tc4ROCJYbm0'));
        $this->assertTrue($response->isSuccessful());
        $data = json_decode($response->content());
        $this->assertEquals($data->title, "AT&T Archives: The UNIX Operating System");
        $this->assertEquals($data->duration, "00:27:27");
        $this->assertEquals($data->youtubeid, "tc4ROCJYbm0");
    }
    
    public function testDuplicateUrl(){
       $this->action('POST', 'VideoController@store', array('url' => 'https://www.youtube.com/watch?v=tc4ROCJYbm0'));
        $response = $this->action('POST', 'VideoController@store', array('url' => 'https://www.youtube.com/watch?v=tc4ROCJYbm0'));
        $response = json_decode($response->content());
        $this->assertFalse($response->verification);
    }
}
