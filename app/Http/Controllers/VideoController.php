<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Youtube;
use App\Http\Controllers\Controller;
use App\Video;
use Config;
use Input;
use DateInterval;
use Exception;
use DateTimeImmutable;

class VideoController extends Controller {

    /**
     * Converts a DateInterval Object to a duration in seconds
     * 
     * @param DateInterval $dateInterval
     * @return int seconds
     */
    protected static function dateIntervalToSeconds(DateInterval $dateInterval) {
        $reference = new DateTimeImmutable;
        $endTime = $reference->add($dateInterval);
        return $endTime->getTimestamp() - $reference->getTimestamp();
    }

    /**
     * This function adds autoplay to a youtube frame
     * 
     * @param String $player
     */
    public static function enableAutoPlay($playerHtml) {
        $stringArray = explode(' ', $playerHtml);
        $length = strlen($stringArray[3]);
        $stringArray[3] = substr_replace($stringArray[3], '?autoplay=1"', $length - 1);
        $embededHtml = implode(' ', $stringArray);
        return $embededHtml;
    }

    /**
     * Formats the duration in h:m:s
     * 
     * The input is the duration in seconds
     * 
     * @param int $duration
     */
    protected static function formatDuration($duration) {
        return gmdate('H:i:s', $duration);
    }

    protected $youtube;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct() {
        $key = Config::get('youtube.KEY');
        $this->youtube = new Youtube($key);
    }

    protected static function createVerifyMessage($value, $message) {
        if ($value === false) {
            return json_encode(['verification' => $value, 'message' => $message]);
        } else {
            return json_encode(['verification' => $value]);
        }
    }

    protected function parseVideoIdFromInput() {
        $youtubeUrl = Input::get("url");
        return $this->youtube->parseVIdFromURL($youtubeUrl);
    }

    public function index() {
        //
        return view('videoList');
    }

    protected static function makeArrayForJsonEncode(Video $video) {
        $embededHtml = static::makeThumbnailHtml($video->thumbnail);
        $title = $video->title;
        $duration = static::formatDuration($video->duration);
        $description = $video->description;
        $youtubeid = $video->youtubeid;
        return compact('title', 'duration', 'description', 'embededHtml', 'youtubeid');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
//    public function create() {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * This method asks the youtube api for the data of the video and stores it to the database.
     * It stores the title, description, duration, youtube id and a link to the thumbnail.
     * 
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //parse Data from youtube api
        try {
            $id = $this->parseVideoIdFromInput();
            if (!$this->checkDuplicate($id)) {
                $videoObject = $this->youtube->getVideoInfo($id);
                if($videoObject == null){
                    return static::createVerifyMessage(false, 'Invalid URL');
                }
                $videoTitle = $videoObject->snippet->title;
                $description = $videoObject->snippet->description;
                $lengthString = $videoObject->contentDetails->duration;
                $thumbnailLink = $videoObject->snippet->thumbnails->high->url;
                //put the data in the VideoModel
                $dateInterval = new DateInterval($lengthString);
                $videoModel = new Video();
                $videoModel->title = $videoTitle;
                $videoModel->description = $description;
                $videoModel->thumbnail = $thumbnailLink;
                $videoModel->duration = static::dateIntervalToSeconds($dateInterval);
                $videoModel->youtubeid = $id;
                $videoModel->save();
                return json_encode(static::makeArrayForJsonEncode($videoModel));
            } else {
                return static::createVerifyMessage(false, 'Duplicate entry');
            }
        } catch (Exception $exception) {
            return static::createVerifyMessage(false, 'Invalid URL');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function show($id) {
//        //
//        echo "show";
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
//    public function edit($id) {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
//    public function update(Request $request, $id) {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
//    public function destroy($id) {
//        //
//    }

    /**
     * Reads an Youtube URL from the input iwth the key youtubeid
     * 
     * @return String
     */
    public function verification() {
        //if the id is invalid an exception is thrown
        $valid = true;
        $message = "Invalid URL";
        try {
            $id = $this->parseVideoIdFromInput();
            $valid = $this->verifyId($id);
            if ($this->checkDuplicate($id)) {
                $message = 'Duplicate Entry';
                $valid = false;
            }
        } catch (Exception $exception) {
            $valid = false;
        }
        return static::createVerifyMessage($valid, $message);
    }

    protected function verifyId($id) {
        $valid = true;
        $videoObject = $this->youtube->getVideoInfo($id);
        if ($videoObject == null) {
            $valid = false;
        }
        return $valid;
    }

    protected function checkDuplicate($id) {
        $isDouble = false;
        $count = Video::where('youtubeid', $id)->count();
        if ($count > 0) {
            $isDouble = true;
        }
        return $isDouble;
    }

    protected static function makeThumbnailHtml($link) {
        return "<img src='$link'><div class='overlay'></div>";
    }

    /**
     * This Function gets all the Videos from the database and formats them as JSON
     * 
     * 
     * @return JSON
     */
    public function returnVideosAsJson() {
        $resultList = Video::orderBy('created_at', 'DESC')->get();
        //process the data from the database and give it to the view
        $videoList = array();
        foreach ($resultList as $video) {
            array_push($videoList, static::makeArrayForJsonEncode($video));
        }
        return view('VideoJSONList')->with('videoList', $videoList);
    }

    /**
     * Returns a HTML player snippet iwth enabled autoplay
     * 
     * The input is the youtube id by the key 'youtubeid'.
     * 
     * 
     * @return type
     */
    public function getPlayerById() {
        $youtubeid = Input::get('youtubeid');
        $videoObject = $this->youtube->getVideoInfo($youtubeid);
        $embededHtml = $videoObject->player->embedHtml;
        $embededHtml = static::enableAutoPlay($embededHtml);
        return $embededHtml;
    }

}
