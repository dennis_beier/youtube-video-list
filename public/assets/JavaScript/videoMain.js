

var app = angular.module('videoList', []);
//shared data for the both controllers, contains the list of videos
app.factory('VideoModel', function () {
    var VideoModel = {};
    VideoModel.videoList = [];
    return VideoModel;
});
//marks the the html as trusted, else angular would not display it
app.filter("sanitize", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);
app.controller('NewVideoController', function NewVideoController($scope, $http, VideoModel) {
    //shared data model
    $scope.VideoModel = VideoModel;
    //class for the inputfield, changes when a valid url is entered
    $scope.verificationClass = 'not-validatet';
    //error message under the input field
    $scope.message = '';
    //submit button enabled or disabled
    $scope.buttonDisabled = true;
    //creates a new video
    $scope.newVideo = function ($event) {
        $http.post(base_url + "video", {
            "_token": token,
            "url": $scope.video.url,
            "_methode": "POST"
        }).success(function (data, status, headers, config) {
            //add the new created video to the first position of the video list
            $scope.VideoModel.videoList.unshift(data);
            //empty the url input field
            $scope.video.url = '';
        }).error(function (status, headers, config) {
            $scope.message = 'no connection to server';
        });
    };
    $scope.verifyAdress = function ($event) {
        $http({
            url: base_url + "video/verification",
            method: "GET",
            params: {"_token": token, "url": $scope.video.url}
        }).success(function (data) {
            var successObject = data;
            if (successObject.verification === true) {
                $scope.verificationClass = 'validatet';
                $scope.buttonDisabled = false;
                $scope.message = '';
            } else {
                $scope.verificationClass = 'not-validatet';
                $scope.message = successObject.message;
                $scope.buttonDisabled = true;
            }
        }).error(function (status, headers, config) {
            $scope.message = 'no connection to server';
        });
    };
});
app.controller("VideoListController", function VideoListController($scope, $http, VideoModel) {
    $scope.VideoModel = VideoModel;
    //get the video data as json
    $http.get(base_url + 'video/getlist').
            success(function (data, status, headers, config) {
                $scope.VideoModel.videoList = data;
            })
    $scope.videoLimit = 12;
    //get the embedded iframe code for the youtube id
    $scope.play = function (video) {
        $http({
            url: base_url + "video/getEmbededCode",
            method: "GET",
            params: {"_token": token, "youtubeid": video.youtubeid}
        }).success(function (data, status, headers, config) {
            video.embededHtml = data;
        }).error(function (status, headers, config) {
            $scope.message = 'no connection to server';
        });
    }
    //show more videos
    $scope.incrementLimit = function () {
        $scope.videoLimit += 12;
    }
});