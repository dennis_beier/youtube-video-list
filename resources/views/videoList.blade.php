<!DOCTYPE html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js"></script>
    <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular-sanitize.min.js"></script>-->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
    <script type="text/javascript" src="{{asset('assets/JavaScript/videoMain.js')}}"></script>
    <link href="{{asset('assets/style/video.css')}}" rel="stylesheet"></link>
    <script type="text/javascript">
        //security token, protection against spam
        var token = "{{csrf_token()}}";
        var base_url = "{{URL::to('/')}}/";
    </script>
    <style>
        .clock{
            background-image: url("{{asset('assets/images/clock.png')}}");
        }
        .overlay{
            background-image: url("{{asset('assets/images/YouTube-icon-full_color.png')}}");
        }
    </style>
</head>
<body ng-app="videoList">
    <div id="content-wrapper" >
        <div id='error'>

        </div>
        <!--Form for the url input-->
        <form id="new-video-form" ng-controller="NewVideoController">
            <div id="input-container">
                <input class="video-url" type="text" name="youtubeUrl" ng-model="video.url" ng-keyup="verifyAdress($event)" ng-class="verificationClass">
                <div id="verfication" ng-class="verificationClass" ng-show="video.url.length"> @{{message}} </div>
            </div>
            <button class='submit' ng-click="newVideo($event)" ng-disabled="buttonDisabled">Submit</button>
        </form> 

        <!--Template for the video files-->
        <div id="video-list" ng-controller="VideoListController" >
            <!--Limit number of videos displayed-->
            <div class="video-container" ng-repeat="video in VideoModel.videoList| limitTo: videoLimit">
                <p class="video-title">@{{video.title}}</p> 
                <div class="embededHtml" ng-bind-html="video.embededHtml | sanitize" ng-click="play(video)"></div>
                <p class="description"> @{{ video.description | limitTo: 200 }}<span ng-if="video.description.length > 200">…</span> </p>
                <span class="video-duration"><div class="clock"></div>@{{ video.duration}}</span>
            </div>
            <p class="more-videos" ng-if="videoLimit < VideoModel.videoList.length" ng-click="incrementLimit()">load more</p>
            <!--Error message when the videos cannot be loaded-->
            <div id="error-message" ng-show="message.length"> @{{message}} </div>
        </div>

    </div>
</body>